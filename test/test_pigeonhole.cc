#include <catch2/catch.hpp>
#include <fmt/core.h>
#include <iostream>

#include "minisat/core/Solver.h"

constexpr bool Verbose = true;

using namespace Minisat;

Var toVar(int N_holes, int pigeon, int hole) { return pigeon * N_holes + hole; }

void initVars(Solver &solver, int N_pigeons, int N_holes) {
  for (int i = 0; i < N_pigeons; ++i) {
    for (int j = 0; j < N_holes; ++j) {
      solver.setVarName(solver.newVar(/*upol=*/l_False),
                        fmt::format("{:d}{:c}", i, 'A' + j));
    }
  }
}

void mutuallyExclusive(Solver &solver, const vec<Lit> &literals, bool check) {
  for (int i = 0; i < literals.size(); ++i) {
    for (int j = i + 1; j < literals.size(); ++j) {
      if (check) {
        REQUIRE((solver.modelValue(literals[i]) == l_False ||
                 solver.modelValue(literals[j]) == l_False));
      } else {
        solver.addClause(~literals[i], ~literals[j]);
      }
    }
  }
}

void exactlyOneTrue(Solver &solver, const vec<Lit> &literals, bool check) {
  if (check) {
    REQUIRE(std::any_of(
        static_cast<Lit const *>(literals),
        literals.size() + static_cast<Lit const *>(literals),
        [&solver](Lit l) { return solver.modelValue(l) == l_True; }));
  } else {
    solver.addClause(literals);
  }
  mutuallyExclusive(solver, literals, check);
}

void pigeonHoles(Solver &solver, int N_pigeons, int N_holes, bool check) {
  for (int i = 0; i < N_pigeons; ++i) {
    vec<Lit> literals;
    for (int j = 0; j < N_holes; ++j) {
      literals.push(mkLit(toVar(N_holes, i, j)));
    }
    // Exactly one of the holes must be selected for pigeon i
    exactlyOneTrue(solver, literals, check);
  }

  for (int j = 0; j < N_holes; ++j) {
    vec<Lit> literals;
    for (int i = 0; i < N_pigeons; ++i) {
      literals.push(mkLit(toVar(N_holes, i, j)));
    }
    // No two pigeons in one hole
    mutuallyExclusive(solver, literals, check);
  }
}

bool solvePigeonHoles(int N_pigeons, int N_holes) {
  Solver solver;
  if (Verbose) {
    solver.verbosity = 9;
  }
  initVars(solver, N_pigeons, N_holes);
  pigeonHoles(solver, N_pigeons, N_holes, /*check=*/false);
  bool ret = solver.solve();
  if (Verbose) {
    std::cerr << "xzl ret=" << ret << std::endl;
  }
  if (ret) {
    if (Verbose) {
      for (int i = 0; i < N_pigeons; ++i) {
        for (int j = 0; j < N_holes; ++j) {
          std::cerr << toInt(solver.modelValue(mkLit(toVar(N_holes, i, j))))
                    << " ";
        }
        std::cerr << std::endl;
      }
    }
    pigeonHoles(solver, N_pigeons, N_holes, /*check=*/true);
  }
  return ret;
}

TEST_CASE("Pigeon holes", "[pigeon]") {
  SECTION("SAT cases") {
    auto [pigeons, holes] = GENERATE(table<int, int>({
        {0, 1},
        {1, 1},
        {0, 2},
        {1, 2},
        {2, 2},
        {4, 5},
        {5, 5},
        {19, 20},
        {20, 20},
        {25, 25},
    }));

    REQUIRE(solvePigeonHoles(pigeons, holes));
  }

  SECTION("UNSAT cases") {
    auto [pigeons, holes] = GENERATE(table<int, int>({
        {7, 6},
        {1, 0},
        {2, 0},
        {2, 1},
        {4, 3},
        {5, 4},
        {8, 7},
        {9, 8},
    }));
    REQUIRE_FALSE(solvePigeonHoles(pigeons, holes));
  }
}

#ifdef CATCH_CONFIG_ENABLE_BENCHMARKING
TEST_CASE("Benchmark Pigeon holes", "[benchmark][pigeon]") {
  SECTION("Benchmark UNSAT cases") {
    auto [pigeons, holes] = GENERATE(table<int, int>({
        {7, 6},
        {9, 8},
        {10, 9},
        {11, 10},
        // {12, 11},
        // {13, 12},
    }));

    BENCHMARK(fmt::format("UNSAT {} pigeons {} holes", pigeons, holes)) {
      Solver solver;
      if (Verbose) {
        solver.verbosity = 9;
      }
      initVars(solver, pigeons, holes);
      pigeonHoles(solver, pigeons, holes, /*check=*/false);
      bool result = solver.solve();
      REQUIRE_FALSE(result);
      return result;
    };
  }
}
#endif // CATCH_CONFIG_ENABLE_BENCHMARKING
