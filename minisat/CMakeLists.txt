find_package(ZLIB)
find_package(fmt)

add_library(
  minisat-lib OBJECT
  utils/Options.cc
  utils/System.cc
  core/Solver.cc
  simp/SimpSolver.cc)

target_link_libraries(minisat-lib PUBLIC project_warnings project_options fmt::fmt)
target_include_directories(minisat-lib PUBLIC ${minisat_SOURCE_DIR})
set_target_properties(minisat-lib PROPERTIES POSITION_INDEPENDENT_CODE ON)

file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/_null.cpp "")
add_library(minisat-lib-static STATIC ${CMAKE_CURRENT_BINARY_DIR}/_null.cpp)
add_library(minisat-lib-shared SHARED ${CMAKE_CURRENT_BINARY_DIR}/_null.cpp)

target_link_libraries(
  minisat-lib-static
  PUBLIC project_warnings
         project_options
         minisat-lib
         ${ZLIB_LIBRARY})
target_link_libraries(
  minisat-lib-shared
  PUBLIC project_warnings
         project_options
         minisat-lib
         ${ZLIB_LIBRARY})

add_executable(minisat_core core/Main.cc)
add_executable(minisat_simp simp/Main.cc)

if(STATIC_BINARIES)
  target_link_libraries(
    minisat_core
    project_warnings
    project_options
    minisat-lib-static)
  target_link_libraries(
    minisat_simp
    project_warnings
    project_options
    minisat-lib-static)
else()
  target_link_libraries(
    minisat_core
    project_warnings
    project_options
    minisat-lib-shared)
  target_link_libraries(
    minisat_simp
    project_warnings
    project_options
    minisat-lib-shared)
endif()

set_target_properties(minisat-lib-static PROPERTIES OUTPUT_NAME "minisat")
set_target_properties(
  minisat-lib-shared
  PROPERTIES OUTPUT_NAME "minisat"
             VERSION ${MINISAT_VERSION}
             SOVERSION ${MINISAT_SOVERSION})

# --------------------------------------------------------------------------------------------------
# Installation targets:

include(GNUInstallDirs)

install(
  TARGETS minisat-lib-static
          minisat-lib-shared
          minisat_core
          minisat_simp
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})

install(
  DIRECTORY mtl
            utils
            core
            simp
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/minisat
  FILES_MATCHING
  PATTERN "*.h")
